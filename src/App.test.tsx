import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  test('renders loading component initially', () => {
    render(<App />);
    expect(screen.getByTestId('loading-component')).toBeInTheDocument();
  });

  // test('renders header and home components initially', () => {
  //   const initLoad = true;
  //   render(<App />);
  //   expect(screen.getByTestId('header-component')).toBeInTheDocument();
  //   expect(screen.getByTestId('home-component')).toBeInTheDocument();
  // })
});
