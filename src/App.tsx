import React, { useEffect, useState } from 'react';
import './App.css';
import Home from './pages/Home';
import Header from './components/ui/Header';
import Loading from './pages/Loading';

const App = () => {
  const [initLoad, setInitLoad] = useState(true);

	useEffect(() => {
		const delay = setTimeout(() => {
      setInitLoad(false);
      clearTimeout(delay)
    }, 5500);
	}, [])

  return (
    <div className="App">
      {
        !initLoad &&
        <>
          <Header/>
          <Home/>
        </>
      }
      {
        initLoad && <Loading />
      }
    </div>
  );
}

export default App;
