import { useState, useEffect } from 'react';

interface Props {
    text: string;
		classStr: string;
}

const TypingLoader: React.FC<Props> = ({ text, classStr }) => {
  const [{content, ctr}, setContent] = useState({
		content: '',
		ctr: 0
	})
       
  useEffect(() => {
    if(ctr === text.length) return

    const delay = setTimeout(() => {
      setContent({content: content + text[ctr], ctr: ctr+1})
      clearTimeout(delay)
    }, 0 | (Math.random() * 100));

  }, [content])
  
  return <span role="loader" className={classStr}> { content } </span>
};

export default TypingLoader;
