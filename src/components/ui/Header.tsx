import mask from '../../assets/images/theater-mask.svg'

const Header: React.FC = () => {
	return (
		<div data-testid="header-component" className="sticky top-0 flex items-center justify-between flex-wrap bg-white p-6">
			<div className="flex items-center flex-shrink-0 text-black mr-6">
				<img src={mask} alt=""/>
				<span className="font-semibold text-sm tracking-tight ps-1">Tell Me A Joke</span>
			</div>
		</div>
	)
}

export default Header;