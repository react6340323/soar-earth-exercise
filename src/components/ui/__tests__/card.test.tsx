import { render, screen } from '@testing-library/react';
import Card from '../Card';

describe('Card component', () => {
  test('renders the joke count and length', () => {
    const data = 'This is a test joke.';
    const count = 1;
    render(<Card data={data} count={count} />);

    const countElement = screen.getByText('Joke #1');
    expect(countElement).toBeInTheDocument();

    const lengthElement = screen.getByText(`Characters: ${data.length}`);
    expect(lengthElement).toBeInTheDocument();
  });

  test('renders the joke text', () => {
    const data = 'This is a test joke.';
    const count = 1;
    render(<Card data={data} count={count} />);

    const textElement = screen.getByText(data);
    expect(textElement).toBeInTheDocument();
  });
});
