import { render, screen } from '@testing-library/react';
import TypingLoader from '../TypingLoader';

describe('TypingLoader component', () => {
  test('displays the text content with typing effect', async () => {
    const text = 'Loading...';
    const classStr = 'loader';
    render(<TypingLoader text={text} classStr={classStr} />);

    const loaderElement = screen.getByRole("loader");
    expect(loaderElement).toBeInTheDocument();

    await screen.findByText(text);

    expect(loaderElement).toHaveTextContent(text);
  });
});
