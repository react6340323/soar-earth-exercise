import { render, screen, fireEvent } from '@testing-library/react';
import Error from '../Error';

describe('Error component', () => {
  test('renders the error message', () => {
    const error = 'An error occurred while fetching jokes.';
    render(<Error error={error} />);

    // Assert error message
    const errorMessage = screen.getByText(error);
    expect(errorMessage).toBeInTheDocument();
  });

  test('expands/collapses the error message on button click', () => {
    const error = 'An error occurred while fetching jokes.';
    render(<Error error={error} />);

    // Assert initial state (collapsed)
    const collapsedMessage = screen.queryByText(error);
    expect(collapsedMessage).toBeInTheDocument();

    // Click "Read more" button to expand
    const readMoreButton = screen.getByText('Read more');
    fireEvent.click(readMoreButton);

    // Assert expanded state
    const expandedMessage = screen.getByText(error);
    expect(expandedMessage).toBeInTheDocument();

    // Click "Read less" button to collapse
    const readLessButton = screen.getByText('Read less');
    fireEvent.click(readLessButton);

    // Assert collapsed state
    const collapsedMessageAfterCollapse = screen.queryByText(error);
    expect(collapsedMessageAfterCollapse).toBeInTheDocument();
  });
});
