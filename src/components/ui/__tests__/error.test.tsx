import { render, screen, fireEvent } from '@testing-library/react';
import Error from '../Error';

describe('Error component', () => {
  test('renders error message and toggles content visibility', () => {
    const error = 'An error occurred';
    render(<Error error={error} />);

    const errorMessage = screen.getByText(/there's been an error/i);
    expect(errorMessage).toBeInTheDocument();

    const collapsedContent = screen.queryByText(error);
    expect(collapsedContent).toBeInTheDocument();

    const toggleButton = screen.getByText(/read more/i);
    fireEvent.click(toggleButton);

    const expandedContent = screen.getByText(error);
    expect(expandedContent).toBeInTheDocument();

    fireEvent.click(toggleButton);

    const collapsedContentAfterToggle = screen.queryByText(error);
    expect(collapsedContentAfterToggle).toBeInTheDocument();
  });
});
