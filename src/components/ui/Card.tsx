interface Props {
	data: string
	count: number
}

const Card: React.FC<Props> = ({ data, count }) => {
	return (
		<div className="col-span-1 flex flex-col bg-white border-2 p-4 rounded-xl overflow-hidden shadow-lg border-[#fefce8] bg-white">
			<div className="px-6 py-4">
				<div className="font-bold text-xl mb-2">Joke #{count}</div>
				<p className="text-xs mb-4">Characters: {data?.length}</p>
				<hr/>
				<div className="flex items-center justify-center w-70">
					<span className={`text-gray-700 text-${data?.length > 360 ? '[0.75rem]' : 'base'} mt-5`}>
						{ data }
					</span>
				</div>
			</div>
		</div>
	)
};

export default Card;