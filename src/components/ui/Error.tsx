import { useState } from "react";
import skull from "../../assets/images/skull.svg"

interface Props {
	error: string;
}

const Error: React.FC<Props> = ({ error }) => {
	const [isCollapsed, setIsCollapsed] = useState(true);

	const toggleCollapse = () => {
    setIsCollapsed(!isCollapsed);
  };

	return (
		<div className="">
			<div className="flex justify-center mb-10">
				<img src={skull} height={70} width={70} alt=""/>
			</div>
			<div className="">
				<p className="w-80">I know it's not funny, but there's been an error retrieving our jokes.</p>

				<button
					className="text-blue-500 hover:text-blue-700 mt-5"
					onClick={toggleCollapse}
				>
					{isCollapsed ? 'Read more' : 'Read less'}
				</button>

				<div
					className={`mt-2 ${isCollapsed ? 'hidden' : 'block'}`}
				>
					<p className="text-gray-600">
						{ error }
					</p>
				</div>
			</div>
		</div>
	)
}

export default Error;