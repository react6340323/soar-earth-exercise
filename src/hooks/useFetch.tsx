import { useState, useEffect, useRef } from "react";


export const useFetch = (url: string, counter: number, init: boolean) => {
  const [data, setData] = useState([] as string[]);
	const [error, setError] = useState("");
	const [loading, setLoading] = useState(false);
	const dataFetchedRef = useRef(false);

  const fetchData = async () => {
		setLoading(true);
		setError("");

		await fetch(url+`&amount=${counter}`)
			.then(response => response.json())
			.then((data) => setData(data.jokes.map((x: { joke: string; }) => x.joke)))
			.catch((error) => {
				const err = new Error(error);
				console.log(err.message);
				setError(err.message)
			})
			.finally(() => setLoading(false));
  };

  useEffect(() => {
		if (init) {
			if (dataFetchedRef.current) return;
			dataFetchedRef.current = true;
			fetchData();
		}
  });

  return { data, error, loading, fetchData };
};