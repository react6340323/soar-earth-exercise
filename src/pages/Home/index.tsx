import { useEffect, useState } from "react";
import Card from "../../components/ui/Card";
import { useFetch } from "../../hooks/useFetch";
import { sortArray } from "../../utils/utilFxn";
import TypingLoader from "../../components/ui/TypingLoader";
import Error from "../../components/ui/Error";

const Home: React.FC = () => {
	const apiUrl = "https://v2.jokeapi.dev/joke/Programming?type=single";
	const [jokes, setJokes] = useState([] as string[]);
	const [isGenerated, setIsGenerated] = useState(false);
	const { data, loading, error, fetchData } = useFetch(apiUrl, 3, false);

	useEffect(() => {
		if (data && data.length > 0) {
			console.log("DATA", data);
			setJokes(sortArray(data));
		}
	}, [data]);

	useEffect(() => {
		if (error) {
			setIsGenerated(false);
		}
	}, [error]);

	const handleClick = () => {
		fetchData().then(() => {
			setIsGenerated(true);
		})
	}
	
	return (
		<div data-testid="home-component" className="h-[90vh] bg-[#F6F8FA] p-10">
			<button
				className={`transition-all duration-500 ease-in-out mt-[30vh] py-4 px-10 shadow-md no-underline rounded-full bg-blue text-black font-bold text-lg font-sans text-sm border-blue bg-[#fcd34d] hover:bg-[#fde047] focus:outline-none active:shadow-none mr-2 ${
					(isGenerated || error) ? 'transform translate-y-[-25vh]' : ''
				}`}
				onClick={handleClick}
				data-testid='generate'
			>
				{
					(!loading && !isGenerated) &&
					<span>Generate</span>
				}
				{
					loading &&
					<div>
						<span>Cracking</span><TypingLoader text="..." classStr=""/>
						<div
							className="inline-block h-4 w-4 ms-2 animate-spin rounded-full border-2 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]">
						</div>
					</div>
				}
				{
					(!loading && isGenerated) && 
					<span>Retry</span>
				}
			</button>
			{
				(!loading && !isGenerated && !error) &&
				<p className="mt-5 text-[0.75rem]">We both know you need some more humor in life, so let me tell you some jokes.</p>
			}
			{
				error && 
				<div className="flex justify-center items-center mt-[-10vh]">
					<Error error={error}/>
				</div>
			}
			{
				(!error && !loading && isGenerated) && 
				<div className="p-6 grid md:grid-cols-3 gap-4 mt-[-20vh]">
					<Card data={jokes[0]} count={1}/>
					<Card data={jokes[1]} count={2}/>
					<Card data={jokes[2]} count={3}/>
				</div>
			}
		</div>
	);
};

export default Home;