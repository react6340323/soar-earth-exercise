import { render, screen, fireEvent } from '@testing-library/react';
import Home from '../index';

test('renders Home component and handles click event', () => {
  render(<Home />);

  expect(screen.getByTestId('home-component')).toBeInTheDocument();
  expect(screen.getByTestId('generate')).toBeInTheDocument();

  fireEvent.click(screen.getByTestId('generate'));
  expect(screen.getByText('Cracking')).toBeInTheDocument();

//   expect(screen.getByText('Retry')).toBeInTheDocument();
});
