import { render, screen } from '@testing-library/react';
import Loading from '../index';

test('renders Loading component', () => {
  render(<Loading />);
  
  expect(screen.getByTestId('loading-component')).toBeInTheDocument();
  expect(screen.getByRole('img')).toBeInTheDocument();
  expect(screen.getByText('Tell Me A Joke')).toBeInTheDocument();
  expect(screen.getByTestId('typing-text')).toBeInTheDocument();
});
