import TypingLoader from "../../components/ui/TypingLoader";
import mask from '../../assets/images/theater-mask.svg'

const Loading: React.FC = () => {
  return (
		<div data-testid="loading-component" query-testid="loading-component" className="h-screen bg-white p-10 pt-[30vh]">
			<div className="flex items-center justify-center flex-shrink-0 text-black mr-6">
				<img src={mask} height={60} width={60} alt=""/>
				<span className="font-semibold text-lg tracking-tight ps-1">Tell Me A Joke</span>
			</div>
			<div className="mt-5" data-testid="typing-text">
				<TypingLoader classStr="" text="Don’t worry if it doesn’t work right. If everything did, you’d be out of a job."/>
			</div>
			
		</div>
	)
}

export default Loading;