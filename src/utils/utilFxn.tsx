export const sortArray = (arr: string[]) => {
	return arr.sort((a: string, b: string) => a.length - b.length);
}